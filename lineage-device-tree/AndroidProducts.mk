#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ampere.mk

COMMON_LUNCH_CHOICES := \
    lineage_ampere-user \
    lineage_ampere-userdebug \
    lineage_ampere-eng
