#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from ampere device
$(call inherit-product, device/amlogic/ampere/device.mk)

PRODUCT_DEVICE := ampere
PRODUCT_NAME := omni_ampere
PRODUCT_BRAND := Amlogic
PRODUCT_MODEL := X96mini_P
PRODUCT_MANUFACTURER := amlogic

PRODUCT_GMS_CLIENTID_BASE := android-amlogic

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="ampere-userdebug 9 PPR1.180610.011 20190801 test-keys"

BUILD_FINGERPRINT := Amlogic/ampere/ampere:9/PPR1.180610.011/20190801:userdebug/test-keys
