#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/recovery:15878144:43bacd08328866c4f827981d190ad170cfa7b072; then
  applypatch  EMMC:/dev/block/boot:9658368:05aca866e459ddeaf075a4acab950996f4557195 EMMC:/dev/block/recovery 43bacd08328866c4f827981d190ad170cfa7b072 15878144 05aca866e459ddeaf075a4acab950996f4557195:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
